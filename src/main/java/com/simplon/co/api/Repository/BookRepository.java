package com.simplon.co.api.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.simplon.co.api.Entity.Book;


@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
    // Query method in order to create /books/title/{title} endpoint
    List<Book> findByTitle(String title);
}


