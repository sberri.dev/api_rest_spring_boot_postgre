package com.simplon.co.api.Service;

import java.util.List;
import java.util.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import com.simplon.co.api.Entity.Book;
import com.simplon.co.api.Repository.BookRepository;

@Service
public class BookService {

    private BookRepository bookRepository;

    public BookService(BookRepository bookRepositoryInjected) {
        this.bookRepository = bookRepositoryInjected;
    }

    // Here we have several methods
    public Optional<Book> getBookById(Long id) {
        return bookRepository.findById(id);
    }

    public List<Book> getBookByTitle(String title) {
        return this.bookRepository.findByTitle(title);
    }

    public Iterable<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    public Book createBook(Book book) {
        return this.bookRepository.save(book);
    }

    public Book updateBook(Long id, Book book) {
        // Get existing book from db based on id
        Optional<Book> existingBookOptional = bookRepository.findById(id);

        // Check if the book is exists in the db
        if (existingBookOptional.isPresent()) {
            Book existingBook = existingBookOptional.get();

            // Update book's details with informations provided
            existingBook.setTitle(book.getTitle());
            existingBook.setDescription(book.getDescription());
            existingBook.setAvailable(book.isAvailable());

            // Save changes to db and return updated book
            return bookRepository.save(existingBook);

        } else {
            // If no book matching with id found, throw ResponseStatusException with the
            // HttpStatus.NOT_FOUND
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No book is found with this id " + id);
        }
    }

}